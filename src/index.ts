import { getChildrenList, makeDownload } from "./helpers";

const moduleTypeList = ["vod", "resource"] as const;
type ModuleType = typeof moduleTypeList[number];

const getModulesByType = (type: ModuleType) => {
  const moduleSelector =
    "#wrap > section > div.group > div.cont > div > div > ul";
  const typeClassName = (type: ModuleType) =>
    `activity ${type} modtype_${type}`;

  return getChildrenList(moduleSelector)
    .flatMap((it) => [...it.getElementsByClassName(typeClassName(type))])
    .map((it) => ({ id: it.id.split("-")[1], element: it }));
};

const handlers = new Map<ModuleType, (id: string) => () => void>();
handlers.set("vod", (id) => async () => {
  const urlPrefix = "rllhs3774";
  try {
    const content = await (
      await (
        await fetch(
          `https://klms.kaist.ac.kr/mod/vod/viewer/index.php?id=${id}`
        )
      ).blob()
    ).text();

    const link = `https://vod.kaist.ac.kr/rest/stream/${
      content.split(urlPrefix)[1].split("/")[5]
    }/convert;settId=24`;
    makeDownload(link);
  } catch (e) {
    console.error(e);
    alert(`${e}`);
  }
});
handlers.set("resource", (id) => async () => {
  try {
    const response = await fetch(
      `https://klms.kaist.ac.kr/mod/resource/view.php?id=${id}`
    );
    if (response.redirected) {
      makeDownload(response.url);
    } else {
      const content = await (await response.blob()).text();
      const parser = new DOMParser();
      const elem = parser.parseFromString(content, "text/html");
      const link = (elem.querySelector(
        "div.resourceworkaround > a"
      ) as HTMLAnchorElement).href;
      makeDownload(link);
    }
  } catch (e) {
    console.error(e);
    alert(`${e}`);
  }
});

const showDownloadButton = (type: ModuleType, id: string, element: Element) => {
  let actionsElem = element.querySelector(".actions");
  if (actionsElem === null) {
    actionsElem = document.createElement("div");
    actionsElem.classList.add("actions");

    const activityInstanceElem = element.querySelector(".activityinstance")!;
    activityInstanceElem.insertAdjacentElement("afterend", actionsElem);
  }
  let progressTextElem = actionsElem.querySelector(".progress-txt");
  if (progressTextElem === null) {
    progressTextElem = document.createElement("div");
    progressTextElem.classList.add("progress-txt");
    actionsElem.insertAdjacentElement("afterbegin", progressTextElem);
  }

  const downloadStrong = document.createElement("strong");
  downloadStrong.classList.add("pr-2");
  const downloadA = document.createElement("a");
  downloadA.insertAdjacentText("afterbegin", "다운로드");
  downloadA.onclick = handlers.get(type)?.(id) ?? null;
  downloadStrong.insertAdjacentElement("beforeend", downloadA);
  progressTextElem.insertAdjacentElement("afterbegin", downloadStrong);
};

moduleTypeList.forEach((type) => {
  getModulesByType(type).forEach(({ id, element }) => {
    showDownloadButton(type, id, element);
  });
});
