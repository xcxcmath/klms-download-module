export const getChildrenList = (selector: string) => [
  ...(document.querySelector(selector)?.children ?? []),
];
export const makeDownload = (link: string) => {
  const a = document.createElement("a");
  a.href = link;
  a.target = "_blank";
  const e = document.createEvent("MouseEvents");
  e.initMouseEvent(
    "click",
    false,
    false,
    window,
    0,
    0,
    0,
    0,
    0,
    false,
    false,
    false,
    false,
    0,
    null
  );
  a.dispatchEvent(e);
};
